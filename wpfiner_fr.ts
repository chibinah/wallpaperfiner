<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MyDropFrame</name>
    <message>
        <location filename="wpfiner.cpp" line="389"/>
        <source>Drop your image here to fit as desktop wallpaper...</source>
        <translation>Déposez votre image ici...</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="441"/>
        <location filename="wpfiner.cpp" line="451"/>
        <location filename="wpfiner.cpp" line="466"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="442"/>
        <source>Sorry, It seems I can&apos;t read this file (not supported format?): %1
%2</source>
        <translation>Désolé, ce fichier n&apos;a pu être lu (format de fichier non supporté ?): %1
%2</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="452"/>
        <source>Sorry, Image reading was failed: %1</source>
        <translation>Désolé, la lecture du fichier %1 a échoué</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="466"/>
        <source>I can&apos;t load the file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier : %1</translation>
    </message>
</context>
<context>
    <name>WpFinerMainWidget</name>
    <message>
        <location filename="wpfinermw.ui" line="14"/>
        <source>WallpaperFiner (http://hyperprog.com)</source>
        <translation>Wallpaper Finer (http://hyperprog.com)</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="27"/>
        <source>To size:</source>
        <translation>Nouvelle taille :</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="54"/>
        <source>To Desktop</source>
        <translation>Définir comme fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="85"/>
        <location filename="wpfiner.cpp" line="157"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="94"/>
        <location filename="wpfiner.cpp" line="160"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="110"/>
        <location filename="wpfiner.cpp" line="122"/>
        <location filename="wpfiner.cpp" line="184"/>
        <source>Scale down to desktop size</source>
        <translation>Mettre à l&apos;échelle du bureau</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="119"/>
        <location filename="wpfiner.cpp" line="120"/>
        <location filename="wpfiner.cpp" line="163"/>
        <location filename="wpfiner.cpp" line="369"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="wpfinermw.ui" line="127"/>
        <location filename="wpfiner.cpp" line="124"/>
        <location filename="wpfiner.cpp" line="172"/>
        <source>Save in BMP format (Win XP)</source>
        <translation>Enregistrer au format BMP (Win XP)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="112"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="113"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="114"/>
        <source>Info</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="116"/>
        <source>Open image</source>
        <translation>Ouvrir une image</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="117"/>
        <source>Save image</source>
        <translation>Enregistrer l&apos;image</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="119"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="123"/>
        <location filename="wpfiner.cpp" line="166"/>
        <source>Save in JPEG format (Small size)</source>
        <translation>Enregistrer au format JPEG (avec pertes)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="125"/>
        <location filename="wpfiner.cpp" line="178"/>
        <source>Save in PNG format (Lossless)</source>
        <translation>Enregistrer au format PNG (sans pertes)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="219"/>
        <source>Image to fine</source>
        <translation>Image à redimensionner</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="234"/>
        <source>Jpeg file (*.jpg)</source>
        <translation>Fichier JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="236"/>
        <source>Png file (*.png)</source>
        <translation>Fichier PNG (*.png)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="238"/>
        <source>Bmp file (*.bmp)</source>
        <translation>Fichier BMP (*.bmp)</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="240"/>
        <source>Fined wallpaier file to save</source>
        <translation>Fond d&apos;écran redimensionné à sauvegarder</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="256"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="256"/>
        <source>The file has already existed!</source>
        <translation>Ce fichier existe déjà !</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="270"/>
        <location filename="wpfiner.cpp" line="295"/>
        <location filename="wpfiner.cpp" line="313"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="270"/>
        <source>I can&apos;t load the file: %1</source>
        <translation type="unfinished">Impossible d&apos;ouvrir le fichier : %1</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="295"/>
        <source>I can&apos;t write the file: %1</source>
        <translation type="unfinished">Impossible d&apos;enregistrer le fichier : %1</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="313"/>
        <source>I can&apos;t create &quot;WpFiner&quot; directory in your home.</source>
        <translation>Impossible de créer le répertoire &quot;WpFiner&quot; dans votre répertoire personnel.</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="361"/>
        <source>Your wallpaier</source>
        <translation>Votre fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="361"/>
        <source>Sorry, I can&apos;t set the desktop wallpaier.
I saved the file here: %1</source>
        <translation>Impossible de définir le fond d&apos;écran.
L&apos;image a été enregistrée ici : %1</translation>
    </message>
    <message>
        <location filename="wpfiner.cpp" line="370"/>
        <source>&lt;strong&gt;Wallpaper Finer&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;Version: %1&lt;br/&gt;GPL v2&lt;br/&gt;&lt;br/&gt;Author: P&amp;eacute;ter De&amp;aacute;k&lt;br/&gt;Webpage &amp; Contact: &lt;a href=&quot;http://hyperprog.com&quot;&gt;http://hyperprog.com&lt;/a&gt;</source>
        <translation>&lt;strong&gt;Wallpaper Finer&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;Version : %1&lt;br/&gt;GPL v2&lt;br/&gt;&lt;br/&gt;Auteur : De&amp;aacute;k P&amp;eacute;ter&lt;br/&gt;Traduction : nah&lt;br /&gt;Site web et contact : &lt;a href=&quot;http://hyperprog.com&quot;&gt;http://hyperprog.com&lt;/a&gt;</translation>
    </message>
</context>
</TS>
